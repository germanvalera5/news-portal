from django.urls import path
from .views import (
    CompanyDetailView, CompanyEmployeesView, CompanyListViewSet,
    CompanyViewSet,
)


urlpatterns = [
    path('<int:pk>/', CompanyViewSet.as_view(
        {'get': 'retrieve', 'put': 'update'}), name='company-detail'),
    path('', CompanyListViewSet.as_view(
        {'get': 'list', 'post': 'create'}), name='company-list'),
    path('employees/', CompanyEmployeesView.as_view(),
         name='company-list-all'),
    path('mycompany/', CompanyDetailView.as_view(), name='my-company'),
]
