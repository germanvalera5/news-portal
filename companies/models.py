from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField()
    address = models.CharField(max_length=150)
    date_created = models.DateTimeField(auto_now_add=True)
    logo = models.ImageField(upload_to='logos/', null=True, blank=True)

    def __str__(self):
        return self.name
