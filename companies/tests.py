from django.contrib.auth import get_user_model
from django.urls import reverse, resolve
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Company
from .serializers import (
    CompanyDetailSerializer, CompanyCreateSerializer, CompanyListAllSerializer,
    CompanyListSerializer,
)
from .views import (
    CompanyDetailView, CompanyEmployeesView, CompanyListViewSet,
    CompanyViewSet,
)
from posts.models import Post


class CompanyTests(APITestCase):

    admin_email = 'admin@email.com'
    user_email_1 = 'testuser1@email.com'
    user_email_2 = 'testuser2@email.com'
    user_email_3 = 'testuser3@email.com'
    password = 'testpass123'

    def setUp(self):
        """
        Setup test data for company test cases.
        """
        self.company1 = Company.objects.create(
            name='Amazon', url='http://amazon.com', address='USA')
        self.company2 = Company.objects.create(
            name='Google', url='http://amazon.com', address='USA')
        self.company3 = Company.objects.create(
            name='Facebook', url='http://amazon.com', address='USA')
        self.user1 = get_user_model().objects.create_user(
            email=self.user_email_1, password=self.password,
            first_name='John', last_name='Harris',
            company=self.company1, user_type='user',
        )
        self.user2 = get_user_model().objects.create_user(
            email=self.user_email_2, password=self.password,
            first_name='Harry', last_name='Potter',
            company=self.company2, user_type='user', )
        self.user3 = get_user_model().objects.create_user(
            email=self.user_email_3, password=self.password,
            first_name='Ron', last_name='Weasley',
            company=self.company3, user_type='user', )
        self.admin = get_user_model().objects.create_user(
            email=self.admin_email, password=self.password, user_type='admin')
        self.admin.is_staff = True
        self.admin.save()
        self.post_1 = Post.objects.create(
            title='New post of user 1', text='something from user 1',
            user=self.user1, topic='topic of user1', )
        self.post_2 = Post.objects.create(
            title='New post of user 2', text='something from user 2',
            user=self.user2, topic='topic of user2', )
        self.post_3 = Post.objects.create(
            title='New post of user 3', text='something from user 3',
            user=self.user1, topic='topic of user3', )
        self.post_4 = Post.objects.create(
            title='New post of user 4', text='something from user 4',
            user=self.user2, topic='topic of user4', )
        self.companies = Company.objects.all()
        url = reverse('login')
        admin_data = {'email': 'admin@email.com',
                      'password': 'testpass123'}
        user_data_1 = {'email': self.user_email_1,
                       'password': self.password}
        user_data_2 = {'email': self.user_email_2,
                       'password': self.password}
        self.create_data = {'name': 'CHI', 'url': 'http://chi.com',
                            'address': 'Ukraine'}
        self.update_data = {'name': 'EPAM'}
        admin_resp = self.client.post(
            url, admin_data)
        user_resp_1 = self.client.post(
            url, user_data_1)
        user_resp_2 = self.client.post(
            url, user_data_2)
        token1 = admin_resp.data.get('access')
        token2 = user_resp_1.data.get('access')
        token3 = user_resp_2.data.get('access')
        self.admin_token = f"Bearer {token1}"
        self.user_token_1 = f"Bearer {token2}"
        self.user_token_2 = f"Bearer {token3}"

    def test_company_list_view_set_get_for_admin(self):
        """
        Ensure that admin can see list of all companies.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('company-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_list_view_set_post_for_admin(self):
        """
        Ensure that admin can create a company.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('company-list')
        response = self.client.post(url, self.create_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        serializer = CompanyCreateSerializer(self.create_data)
        self.assertEqual(response.data, serializer.data)

    def test_company_list_view_set_get_for_user(self):
        """
        Ensure that user can't get a company list.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('company-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_list_view_set_post_for_user(self):
        """
        Ensure that user can't create company.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('company-list')
        response = self.client.post(url, self.create_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_list_view_set_get_for_non_authorized_user(self):
        """
        Ensure that not authorized user can't get company list.
        """
        url = reverse('company-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_company_list_view_set_post_for_non_authorized_user(self):
        """
        Ensure that not authorized user can't create a company.
        """
        url = reverse('company-list')
        response = self.client.post(url, self.create_data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_company_list_view_set_url_resolves_company_list_view_set(self):
        """
        Ensure that CompanyListViewSet url resolves CompanyListViewSet.
        """
        view = resolve('/api/v1/companies/')
        self.assertEqual(
            view.func.__name__,
            CompanyListViewSet.as_view({'get': 'list',
                                        'post': 'create'}).__name__
        )

    def test_company_view_set_get_for_admin(self):
        """
        Ensure that admin can get a company detail.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('company-detail', args=[self.company3.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = CompanyDetailSerializer(self.company3)
        self.assertEqual(response.data, serializer.data)

    def test_company_view_set_put_for_admin(self):
        """
        Ensure that admin can update company information.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('company-detail', args=[self.company3.pk])
        response = self.client.put(url, self.update_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = CompanyCreateSerializer(
            self.company3, data=self.update_data, partial=True)
        serializer.is_valid()
        serializer.save()
        self.assertEqual(response.data, serializer.data)

    def test_company_view_set_get_for_user(self):
        """
        Ensure that user can't get information about other company.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('company-detail', args=[self.company3.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_view_set_put_for_user(self):
        """
        Ensure that user can't update company information.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('company-detail', args=[self.company3.pk])
        response = self.client.put(url, self.update_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_view_set_get_for_non_authorized_user(self):
        """
        Ensure that not authorized user can't get
        information about other company.
        """
        url = reverse('company-detail', args=[self.company3.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_company_view_set_put_for_non_authorized_user(self):
        """
        Ensure that not authorized user can't update
        company information.
        """
        url = reverse('company-detail', args=[self.company3.pk])
        response = self.client.put(url, self.update_data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_company_view_set_url_resolves_company_view_set(self):
        """
        Ensure that CompanyViewSet url resolves CompanyViewSet.
        """
        view = resolve(f'/api/v1/companies/{self.company3.pk}/')
        self.assertEqual(
            view.func.__name__,
            CompanyViewSet.as_view({'get': 'retrieve',
                                    'put': 'update'}).__name__
        )

    def test_company_employees_view_for_admin(self):
        """
        Ensure that admin can get a list of companies with users information
        and with users posts.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('company-list-all')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_employees_view_for_user(self):
        """
        Ensure that user can't get a list of companies with users informantion
        and with users posts.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('company-list-all')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_employees_view_for_non_authorized_user(self):
        """
        Ensure that not authorized user can't get a list of companies
        with users informantion and with users posts.
        """
        url = reverse('company-list-all')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_company_employees_url_resolves_company_employees_view(self):
        """
        Ensure that CompanyEmployeesView url resolves CompanyEmployeesView.
        """
        view = resolve('/api/v1/companies/employees/')
        self.assertEqual(
            view.func.__name__,
            CompanyEmployeesView.as_view().__name__
        )

    def test_company_detail_view_for_user(self):
        """
        Ensure that user can see information about his company.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('my-company')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = CompanyDetailSerializer(self.user1.company)
        self.assertEqual(response.data, serializer.data)

    def test_company_detail_view_for_non_authorized_user(self):
        """
        Ensure that not authorized user can't see information about company.
        """
        url = reverse('my-company')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_company_detail_url_resolves_company_detail_view(self):
        """
        Ensure that CompanyDetailView url resolves CompanyDetailView.
        """
        view = resolve('/api/v1/companies/mycompany/')
        self.assertEqual(
            view.func.__name__,
            CompanyDetailView.as_view().__name__
        )
