from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Company
from posts.serializers import PostDetailSerializer

User = get_user_model()


class CompanyCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = ('name', 'url', 'address', 'logo')


class CompanyListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'url', 'address', 'logo')


class CompanyDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('name', 'url', 'address', 'logo')


class UserWithPostsSerializer(serializers.ModelSerializer):
    posts = PostDetailSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'user_type', 'first_name',
                  'last_name', 'avatar', 'telephone_number', 'posts')


class CompanyListAllSerializer(serializers.ModelSerializer):
    employees = UserWithPostsSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = ('name', 'url', 'address', 'logo', 'employees')
