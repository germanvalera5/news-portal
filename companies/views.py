from rest_framework.generics import RetrieveAPIView
from rest_framework.mixins import (
    CreateModelMixin, ListModelMixin, RetrieveModelMixin, UpdateModelMixin,
)
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.viewsets import GenericViewSet, generics
from django.contrib.auth import get_user_model
from .serializers import (
    CompanyCreateSerializer, CompanyDetailSerializer,
    CompanyListAllSerializer, CompanyListSerializer,
)
from .models import Company
from users.mixins import MultiSerializerViewSetMixin

User = get_user_model()


class CompanyListViewSet(MultiSerializerViewSetMixin,
                         ListModelMixin,
                         CreateModelMixin,
                         GenericViewSet):
    queryset = Company.objects.all()
    permission_classes = (IsAdminUser, IsAuthenticated, )
    serializer_class = CompanyListSerializer
    serializer_action_classes = {
        'list': CompanyListSerializer,
        'create': CompanyCreateSerializer
    }


class CompanyViewSet(MultiSerializerViewSetMixin,
                     UpdateModelMixin,
                     RetrieveModelMixin,
                     GenericViewSet):
    queryset = Company.objects.all()
    permission_classes = (IsAdminUser, IsAuthenticated, )
    serializer_class = CompanyDetailSerializer
    serializer_action_classes = {
        'update': CompanyCreateSerializer,
        'retrieve': CompanyDetailSerializer
    }

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super().update(request, *args, **kwargs)


class CompanyEmployeesView(generics.ListAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyListAllSerializer
    permission_classes = (IsAdminUser, IsAuthenticated, )


class CompanyDetailView(RetrieveAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyDetailSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        if self.request.user.company:
            self.kwargs['pk'] = self.request.user.company.id
            return super().get_object()
        return
