# News Portal

Simple API for news portal with users and admins for creating posts, companies(only for admins) and users(only for admins).

## Installation

To run this application enter this command:

```
docker-compose -f news-portal/docker-compose.yml up -d 
```
This command start the server and also create 200 users, 10 companies and 30 posts for each user.

## Usage
To create superuser enter this command:
```
docker exec -it news_portal python manage.py createsuperuser
```
After it, you can login as a superuser and create admins.

## Documentation

You can find API documentation [here](http://localhost:8000/swagger/) (works only with running containers) 