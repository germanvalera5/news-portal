from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Post(models.Model):
    title = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='posts')
    text = models.TextField()
    topic = models.TextField()

    def __str__(self):
        return self.title

