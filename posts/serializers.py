from time import time

from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Post

User = get_user_model()


class PostCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('title', 'text', 'topic')

    def create(self, validated_data):
        post = Post.objects.create(
            title=validated_data.get('title'),
            text=validated_data.get('text'),
            topic=validated_data.get('topic'),
            user=self.context.get('request').user
        )
        post.save()
        return post


class PostDetailSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = Post
        fields = ('title', 'text', 'topic', 'user')


class PostListUpdateSerializer(serializers.ListSerializer):
    def update(self, instances, validated_data):
        if validated_data[0]:
            data = validated_data[0]
            post_mapping = {post.id: post for post in instances}
            posts = []
            for post in post_mapping.values():
                for field, value in data.items():
                    setattr(post, field, value)
                posts.append(post)
            Post.objects.bulk_update(posts, [field for field in data.keys()])
            return posts
        else:
            return []


class PostEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('title', 'text', 'topic')
        list_serializer_class = PostListUpdateSerializer


class UserDetailPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name')


class PostListSerializer(serializers.ModelSerializer):
    user = UserDetailPostSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'topic', 'text',  'user')


class PostListUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id', 'title', 'topic', 'text')
