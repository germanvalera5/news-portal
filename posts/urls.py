from django.urls import path
from .views import AllPostsViewSet, PostDetailViewSet

urlpatterns = [
    path('<int:pk>/', PostDetailViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}),
         name='post-detail'),
    path('', AllPostsViewSet.as_view({'get': 'list', 'post': 'create',
                                      'put': 'update'}), name='post-list')
]
