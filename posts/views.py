from rest_framework.mixins import (
    CreateModelMixin, DestroyModelMixin, ListModelMixin, RetrieveModelMixin,
    UpdateModelMixin,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from .filters import PostFilter
from .models import Post
from .permissions import IsAdminOrAuthorOrReadOnly
from .serializers import (
    PostCreateSerializer, PostDetailSerializer, PostEditSerializer,
    PostListSerializer,
)
from users.mixins import MultiSerializerViewSetMixin


class AllPostsViewSet(MultiSerializerViewSetMixin,
                      CreateModelMixin,
                      ListModelMixin,
                      UpdateModelMixin,
                      GenericViewSet):
    queryset = Post.objects.all()
    serializer_class = PostListSerializer
    permission_classes = (IsAuthenticated, )
    serializer_action_classes = {
        'create': PostCreateSerializer,
        'list': PostListSerializer,
        'update': PostEditSerializer,
    }
    filterset_class = PostFilter

    def get_queryset(self):
        if self.request.user.is_staff:
            return Post.objects.all()
        author = self.request.user
        return Post.objects.filter(user=author)

    def update(self, request, *args, **kwargs):
        partial = True
        instance = self.get_queryset()
        serializer = self.get_serializer(instance, data=[request.data],
                                         partial=partial, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class PostDetailViewSet(MultiSerializerViewSetMixin,
                        RetrieveModelMixin,
                        UpdateModelMixin,
                        DestroyModelMixin,
                        GenericViewSet):
    permission_classes = (IsAuthenticated, IsAdminOrAuthorOrReadOnly, )
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    serializer_action_classes = {
        'retrieve': PostDetailSerializer,
        'update': PostEditSerializer,
    }

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super().update(request, *args, **kwargs)
