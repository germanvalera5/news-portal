from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.urls import reverse, resolve
from .models import Post
from .serializers import (
    PostCreateSerializer, PostDetailSerializer, PostEditSerializer,
    PostListSerializer, PostListUserSerializer,
)
from .views import PostDetailViewSet, AllPostsViewSet
from companies.models import Company

User = get_user_model()


class PostTests(APITestCase):

    admin_email = 'admin@email.com'
    user_email_1 = 'testuser1@email.com'
    user_email_2 = 'testuser2@email.com'
    password = 'testpass123'

    def setUp(self):
        """
        Setup data for post test cases.
        """
        self.company = Company.objects.create(
            name='Amazon', url='http://amazon.com', address='USA')
        self.user1 = get_user_model().objects.create_user(
            email=self.user_email_1, password=self.password,
            first_name='John', last_name='Harris',
            company=self.company, user_type='user')
        self.user2 = get_user_model().objects.create_user(
            email=self.user_email_2, password=self.password,
            first_name='Harry', last_name='Potter',
            company=self.company, user_type='user')
        self.admin = get_user_model().objects.create_user(
            email=self.admin_email, password=self.password, user_type='admin')
        self.admin.is_staff = True
        self.admin.save()
        self.post_1 = Post.objects.create(
            title='New post of user 1', text='something from user 1',
            user=self.user1, topic='topic of user1')
        self.post_2 = Post.objects.create(
            title='New post of user 2', text='something from user 2',
            user=self.user2, topic='topic of user2')
        self.post_3 = Post.objects.create(
            title='New post of user 3', text='something from user 3',
            user=self.user1, topic='topic of user3')
        self.post_4 = Post.objects.create(
            title='New post of user 4', text='something from user 4',
            user=self.user2, topic='topic of user4')
        self.valid_post = {
            'title': 'New post of user 2', 'text': 'something from user 2',
            'topic': 'topic of user2'}
        self.invalid_post = {
            'title': '', 'text': 'something from user 2',
            'topic': 'topic of user2'}
        self.data_for_edit = {
            'title': 'new title for post'
        }
        self.posts = Post.objects.all()
        url = reverse('login')
        admin_data = {'email': 'admin@email.com',
                      'password': 'testpass123'}
        user_data_1 = {'email': self.user_email_1,
                       'password': self.password}
        user_data_2 = {'email': self.user_email_2,
                       'password': self.password}
        admin_resp = self.client.post(
            url, admin_data)
        user_resp_1 = self.client.post(
            url, user_data_1)
        user_resp_2 = self.client.post(
            url, user_data_2)
        token1 = admin_resp.data.get('access')
        token2 = user_resp_1.data.get('access')
        token3 = user_resp_2.data.get('access')
        self.admin_token = f"Bearer {token1}"
        self.user_token_1 = f"Bearer {token2}"
        self.user_token_2 = f"Bearer {token3}"

    def test_get_all_posts_as_admin(self):
        """
        Ensure that admin can see all posts of users.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('post-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_posts_as_user(self):
        """
        Ensure that user can see only all his posts.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('post-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        posts = Post.objects.filter(user=self.user1)
        serializer = PostListUserSerializer(posts, many=True)
        self.assertEqual(response.data.get('results'), serializer.data)

    def test_create_post_with_valid_data(self):
        """
        Ensure that post can be created with valid data.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('post-list')
        data = self.valid_post
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        serializer = PostCreateSerializer(self.valid_post)
        self.assertEqual(response.data, serializer.data)

    def test_create_post_with_invalid_data(self):
        """
        Ensure that post can't be created with invalid data.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('post-list')
        data = self.invalid_post
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bulk_update_for_admin(self):
        """
        Ensure that admin can bulk update all posts.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('post-list')
        response = self.client.put(url, self.data_for_edit)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = PostEditSerializer(
            self.posts, data=[self.data_for_edit], many=True, partial=True)
        serializer.is_valid()
        self.assertEqual(response.data, serializer.data)

    def test_bulk_update_for_user(self):
        """
        Ensure that user can bulk update only his own posts.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('post-list')
        response = self.client.put(url, self.data_for_edit)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        posts = Post.objects.filter(user=self.user1)
        serializer = PostEditSerializer(
            posts, data=[self.data_for_edit], many=True, partial=True)
        serializer.is_valid()
        self.assertEqual(response.data, serializer.data)

    def test_all_post_url_resolves_all_post_view_set(self):
        """
        Ensure that AllPostsViewSet url resolves AllPostsViewSet.
        """
        view = resolve('/api/v1/posts/')
        self.assertEqual(
            view.func.__name__,
            AllPostsViewSet.as_view({'get': 'list', 'post': 'create',
                                     'put': 'update'}).__name__
        )

    def test_post_detail_view_set_for_author_get(self):
        """
        Ensure that user can see his own post details.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = PostDetailSerializer(self.post_1)
        self.assertEqual(response.data, serializer.data)

    def test_post_detail_view_set_for_another_user_get(self):
        """
        Ensure user can see post details of another user.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_2)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = PostDetailSerializer(self.post_1)
        self.assertEqual(response.data, serializer.data)

    def test_post_detail_view_set_for_admin_get(self):
        """
        Ensure that admin can see user post details.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = PostDetailSerializer(self.post_1)
        self.assertEqual(response.data, serializer.data)

    def test_post_detail_view_set_for_author_put(self):
        """
        Ensure that user can update his post.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.put(url, self.data_for_edit)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = PostEditSerializer(self.post_1, data=self.data_for_edit, partial=True)
        serializer.is_valid()
        serializer.save()
        self.assertEqual(response.data, serializer.data)

    def test_post_detail_view_set_for_another_user_put(self):
        """
        Ensure that user can't update post of another user.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_2)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.put(url, self.data_for_edit)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_detail_view_set_for_admin_put(self):
        """
        Ensure that admin can update user post.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.put(url, self.data_for_edit)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = PostEditSerializer(self.post_1, data=self.data_for_edit, partial=True)
        serializer.is_valid()
        serializer.save()
        self.assertEqual(response.data, serializer.data)

    def test_post_detail_view_set_for_author_delete(self):
        """
        Ensure that user can delete his own post.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_1)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_post_detail_view_set_for_another_user_delete(self):
        """
        Ensure that user can't delete another user post.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token_2)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_detail_view_set_for_admin_delete(self):
        """
        Ensure that admin can delete user post.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('post-detail', args=[self.post_1.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_post_detail_url_resolves_post_detail_view_set(self):
        """
        Ensure tha PostDetailViewSet url resolve PostDetailViewSet.
        """
        view = resolve(f'/api/v1/posts/{self.post_1.pk}/')
        self.assertEqual(
            view.func.__name__,
            PostDetailViewSet.as_view({'get': 'retrieve', 'put': 'update',
                                       'delete': 'destroy'}).__name__
        )
