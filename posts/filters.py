from django_filters import rest_framework as filters
from posts.models import Post


class PostFilter(filters.FilterSet):
    title = filters.CharFilter(field_name='title', lookup_expr='icontains')
    text = filters.CharFilter(field_name='text', lookup_expr='icontains')
    topic = filters.CharFilter(field_name='topic', lookup_expr='icontains')
    company = filters.CharFilter(field_name='user_id__company__name',
                                 lookup_expr='icontains')

    class Meta:
        model = Post
        fields = ('title', 'text', 'topic', 'company', )
