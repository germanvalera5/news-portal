from django.contrib import admin
from django.contrib.auth import get_user_model

User = get_user_model()


class SoftDeleteAdmin(admin.ModelAdmin):
    """
    List all users in django admin(soft deleted adn not soft deleted)
    """
    list_display = ('id', 'email', 'is_active',)
    list_filter = ('is_active',)

    def get_queryset(self, request):
        qs = self.model.all_objects.all()
        return qs


admin.site.register(User, SoftDeleteAdmin)
