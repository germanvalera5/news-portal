from posts.serializers import PostListUserSerializer


class MultiSerializerViewSetMixin(object):
    """
    Allows using different serializers for different actions.
    Set different 'list' actions for user and admin.
    """

    def get_serializer_class(self):
        try:
            if not self.request.user.is_staff:
                if self.action == 'list':
                    return PostListUserSerializer

            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()
