from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.urls import reverse, resolve
from companies.models import Company
from .views import UserListViewSet, UserViewSet, AdminEditUserViewSet
from .serializers import (
    UserDetailSerializer, UserCreateSerializer, UserEditSerializer,
)

User = get_user_model()


class UserTests(APITestCase):

    admin_email = 'admin@email.com'
    user_email = 'testuser@email.com'
    password = 'testpass123'

    def setUp(self):
        """
        Setup data for user test cases.
        """

        company = Company.objects.create(
            name='Amazon', url='htttp://amazon.com', address='USA')
        self.adminuser = User.objects.create_user(
            email=self.admin_email, password=self.password, user_type='admin')
        self.adminuser.is_staff = True
        self.adminuser.save()
        self.user = User.objects.create_user(
            email=self.user_email, password=self.password,
            first_name='John', last_name='Harris',
            company=company, user_type='user')
        self.admin_data = {
            'email': 'admin@email.com',
            'password': 'testpass123'}
        self.edit_data = {'first_name': 'Harry', 'last_name': 'Potter'}
        self.new_user = {
            'email': 'createduser@email.com',
            'user_type': 'user',
            'password': 'testpass123'}
        self.new_superuser = {
            'email': 'createduser@email.com',
            'user_type': 'superuser',
            'password': 'testpass123'}
        self.new_admin = {
            'email': 'createduser@email.com',
            'user_type': 'admin',
            'password': 'testpass123'}
        url = reverse('login')
        user_data = {'email': 'testuser@email.com', 'password': 'testpass123'}
        admin_resp = self.client.post(url, self.admin_data)
        user_resp = self.client.post(url, user_data)
        token1 = admin_resp.data.get('access')
        token2 = user_resp.data.get('access')
        self.admin_token = f"Bearer {token1}"
        self.user_token = f"Bearer {token2}"

    def test_login_with_valid_data(self):
        """
        Login user and get access and refresh tokens in response.
        """
        url = reverse('login')
        response = self.client.post(url, self.admin_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in response.data)
        self.assertTrue('refresh' in response.data)
        self.assertFalse('something else' in response.data)

    def test_login_with_invalid_data(self):
        """
        Ensure that user can`be logged in with invalid data.
        """
        url = reverse('login')
        data = {'email': 'some@email.com', 'password': 'testpass123'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, {
            "detail": "No active account found with the given credentials"})
        self.assertFalse('This text should not be here.' in response.data)

    def test_user_list_view_for_admin(self):
        """
        Ensure that admin can see list of all users.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('user-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_list_view_for_user(self):
        """
        Ensure that user can't see list of all users.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token)
        url = reverse('user-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            "detail": "You do not have permission to perform this action."})

    def test_user_list_view_for_non_authorized_user(self):
        """
        Ensure that not authorized users can't see list of all users.
        """
        url = reverse('user-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, {
            "detail": "Authentication credentials were not provided."})
        self.assertNotEqual(response.data, {'somerandomtext': 'text'})

    def test_user_list_url_resolves_user_list_view(self):
        """
        Ensure that UserListViewSet url resolves UserListViewSet.
        """
        view = resolve('/api/v1/users/')
        self.assertEqual(
            view.func.__name__,
            UserListViewSet.as_view({
                'get': 'list', 'post': 'create'}).__name__)

    def test_create_user_view_by_admin(self):
        """
        Ensure that user can be created by admin.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('user-list')
        response = self.client.post(url, self.new_user)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        serializer = UserCreateSerializer(self.new_user)
        self.assertEqual(response.data, serializer.data)

    def test_create_admin_by_admin(self):
        """
        Ensure that admin can't create admin.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('user-list')
        response = self.client.post(url, self.new_admin)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_superuser_by_admin(self):
        """
        Ensure that admin can't create superuser.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        url = reverse('user-list')
        response = self.client.post(url, self.new_superuser)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_view_by_user(self):
        """
        Ensure that user can't create user.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token)
        url = reverse('user-list')
        response = self.client.post(url, self.new_user)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            "detail": "You do not have permission to perform this action."})
        self.assertNotEqual(response.data, {'somerandomtext': 'text'})

    def test_user_detail_view_get(self):
        """
        Ensure that user can see his profile information.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token)
        url = reverse('user-detail')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = UserDetailSerializer(self.user)
        self.assertEqual(response.data, serializer.data)
        self.assertNotEqual(response.data.get('first_name'), 'Not a John')

    def test_user_detail_view_update(self):
        """
        Ensure that user can update his profile information.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.user_token)
        url = reverse('user-detail')
        response = self.client.put(url, self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = UserEditSerializer(
            self.user, self.edit_data, partial=True)
        serializer.is_valid()
        serializer.save()
        self.assertEqual(response.data, serializer.data)

    def test_user_detail_view_for_non_authorized_user(self):
        """
        Ensure that non authorized user can't see profile.
        """
        url = reverse('user-detail')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, {
            "detail": "Authentication credentials were not provided."})
        self.assertNotEqual(response.data, {'somerandomtext': 'text'})

    def test_user_detail_url_resolves_user_detail_view(self):
        """
        Ensure that UserViewSet url resolves UserViewSet.
        """
        view = resolve('/api/v1/users/profile/')
        self.assertEqual(
            view.func.__name__,
            UserViewSet.as_view(
                {'get': 'retrieve', 'put': 'update'}).__name__)

    def test_admin_edit_user_view_get(self):
        """
        Ensure that admin can see profile of other user.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        user_pk = self.user.pk
        url = reverse('edit-user', args=[user_pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = UserDetailSerializer(self.user)
        self.assertEqual(response.data, serializer.data)
        self.assertNotEqual(response.data.get('first_name'), 'Harry')

    def test_admin_edit_user_view_put_with_correct_pk(self):
        """
        Ensure that admin can update profile of other user
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        user_pk = self.user.pk
        url = reverse('edit-user', args=[user_pk])
        response = self.client.put(url, self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = UserEditSerializer(
            self.user, self.edit_data, partial=True)
        serializer.is_valid()
        serializer.save()
        self.assertEqual(response.data, serializer.data)

    def test_admin_edit_user_view_put_with_incorrect_pk(self):
        """
        Ensure that put with incorrect pk will get a 404 response.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        user_pk = 1000
        url = reverse('edit-user', args=[user_pk])
        response = self.client.put(url, self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, {'detail': 'Not found.'})

    def test_admin_edit_user_view_soft_delete_with_correct_pk(self):
        """
        Ensure that admin can soft delete user.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        user_pk = self.user.pk
        url = reverse('edit-user', args=[user_pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        all_users = User.objects.all().count() + 1
        self.assertEqual(User.all_objects.all().count(), all_users)

    def test_admin_edit_user_view_soft_delete_with_incorrect_pk(self):
        """
        Ensure that soft delete with incorrect pk will return a 404 response.
        """
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_token)
        user_pk = 1000
        url = reverse('edit-user', args=[user_pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_admin_edit_user_url_resolves_admin_edit_user_view(self):
        """
        Ensure that AdminEditUserViewSet url resolves AdminEditUserViewSet.
        """
        user_pk = self.user.pk
        view = resolve(f'/api/v1/users/{user_pk}/')
        self.assertEqual(
            view.func.__name__,
            AdminEditUserViewSet.as_view(
                {'get': 'retrieve', 'put': 'update',
                 'delete': 'destroy'}).__name__)
