from rest_framework.mixins import (
    CreateModelMixin, DestroyModelMixin, ListModelMixin, RetrieveModelMixin,
    UpdateModelMixin,
)
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from django.contrib.auth import get_user_model
from .mixins import MultiSerializerViewSetMixin
from .serializers import (
    UserCreateSerializer, UserDetailSerializer, UserEditSerializer,
)

User = get_user_model()


class UserViewSet(MultiSerializerViewSetMixin,
                  UpdateModelMixin,
                  RetrieveModelMixin,
                  GenericViewSet):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = UserDetailSerializer
    serializer_action_classes = {
        'update': UserEditSerializer,
        'retrieve': UserDetailSerializer
    }

    def get_object(self):
        self.kwargs['pk'] = self.request.user.pk
        obj = super().get_object()
        return obj


class UserListViewSet(MultiSerializerViewSetMixin,
                      ListModelMixin,
                      CreateModelMixin,
                      GenericViewSet):
    permission_classes = (IsAuthenticated, IsAdminUser,)
    serializer_class = UserDetailSerializer
    serializer_action_classes = {
        'list': UserDetailSerializer,
        'create': UserCreateSerializer
    }

    def get_queryset(self):
        return User.objects.filter(user_type='user')


class AdminEditUserViewSet(MultiSerializerViewSetMixin,
                           UpdateModelMixin,
                           RetrieveModelMixin,
                           DestroyModelMixin,
                           GenericViewSet
                           ):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated, IsAdminUser,)
    serializer_class = UserDetailSerializer
    serializer_action_classes = {
        'update': UserEditSerializer,
        'retrieve': UserDetailSerializer,
    }

    def perform_destroy(self, instance):
        instance.soft_delete()
