from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView,
)
from django.urls import path
from .views import AdminEditUserViewSet, UserListViewSet, UserViewSet

urlpatterns = [
    path('', UserListViewSet.as_view(
        {'get': 'list', 'post': 'create'}), name='user-list'),
    path('profile/', UserViewSet.as_view(
        {'get': 'retrieve', 'put': 'update'}), name='user-detail'),
    path('login/', TokenObtainPairView.as_view(), name='login'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('<int:pk>/', AdminEditUserViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}),
         name='edit-user'),
]
