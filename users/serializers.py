from rest_framework import serializers
from django.contrib.auth import get_user_model
from companies.serializers import CompanyDetailSerializer

User = get_user_model()


class UserDetailSerializer(serializers.ModelSerializer):
    company = CompanyDetailSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'user_type', 'first_name', 'last_name',
                  'avatar', 'telephone_number', 'company')


class UserEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'avatar', 'telephone_number')


class UserCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('email', 'user_type', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, attrs):
        user = self.context.get('request').user
        if user.is_superuser and attrs.get('user_type') == 'admin':
            return attrs
        if attrs.get('user_type') == 'user':
            return attrs
        raise serializers.ValidationError('You do not have permission'
                                          ' to perform this action.')

    def create(self, validated_data):
        user = User(
            email=validated_data.get('email'),
            user_type=validated_data.get('user_type')
        )
        user.set_password(validated_data.get('password'))
        if validated_data.get('user_type') == 'admin':
            user.is_staff = True
        user.save()
        return user
